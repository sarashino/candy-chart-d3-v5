function linechart(){
     var margin = {top: 0, right: 30, bottom: 40, left: 5},
         width = 630, height = 300, MValue = 'MAVG25';

    function linerender(selection){
        selection.each(function(data) {
            var minimal = d3.min(genData, function(d) { return d3.min([d.LOW, d.MAVG25]); });
            var maximal = d3.max(genData, function(d) { return d3.max([d.HIGH, d.MAVG25]); });
            var x = d3.scaleBand()
                    .range([0, width]);
            x.domain(data.map(function(d) { return d.TIMESTAMP; }));
            var y = d3.scaleLinear()
                    .domain([minimal, maximal])
                    .rangeRound([height, 0]);

            var bandwidth = x.bandwidth();
            // 7. d3's line generator
            var line = d3.line()
                .x(function(d) { return x(d.TIMESTAMP) + (bandwidth/2) + 1; }) // set the x values for the line generator
                .y(function(d) { return y(d[MValue]); }) // set the y values for the line generator 
                .curve(d3.curveMonotoneX); // apply smoothing to the line

            // 1. Add the SVG to the page and employ #2
            var svg = d3.select(this).select("svg").append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                        .attr("class", MValue);

            // 9. Append the path, bind the data, and call the line generator 
            svg.append("path")
                .datum(data) // 10. Binds data to the line 
                .attr("class", "line") // Assign a class for styling 
                .attr("d", line); // 11. Calls the line generator 

            // 12. Appends a circle for each datapoint 
            svg.append("g").attr("id", "dots")
                .selectAll(".dot")
                .data(data)
                .enter().append("circle") // Uses the enter().append() method
                .attr("class", "dot") // Assign a class for styling
                .attr("cx", function(d) { return x(d.TIMESTAMP) + (bandwidth/2) + 1 })
                .attr("cy", function(d) { return y(d[MValue]) })
                .attr("r", 2)
        });
    };
    linerender.margin = function(value) {
        if (!arguments.length) return margin.top;
            margin.top = value;
        return linerender;
    };
    return linerender;
}
