var parseDate    = d3.timeParse("%Y-%m-%d");
var TPeriod      = "3M";
var TMAVG        = "MAVG25";
var TDays        = {"1M":21, "3M":63, "6M":126, "1Y":252, "2Y":504, "4Y":1008 };
var TIntervals   = {"1M":"day", "3M":"day", "6M":"day", "1Y":"week", "2Y":"week", "4Y":"month" };
var TFormat      = {"day":"%d %b '%y", "week":"%d %b '%y", "month":"%b '%y" };
var genRaw, genData;
    
(function() {
    d3.csv("stockdata.csv",genType).then(function(data) {
      mainjs(data);
    }); 
}());

function toSlice(data) { return data.slice(-TDays[TPeriod]); }

function mainjs(genRaw) {
  var toPress    = function() { genData = (TIntervals[TPeriod]!="day")?dataCompress(toSlice(genRaw), TIntervals[TPeriod]):toSlice(genRaw);};
  toPress(); displayAll();
  d3.select("#oneM").on("click",   function(){ TPeriod  = "1M"; toPress(); displayAll(); });
  d3.select("#threeM").on("click", function(){ TPeriod  = "3M"; toPress(); displayAll(); });
  d3.select("#sixM").on("click",   function(){ TPeriod  = "6M"; toPress(); displayAll(); });
  d3.select("#oneY").on("click",   function(){ TPeriod  = "1Y"; toPress(); displayAll(); });
  d3.select("#twoY").on("click",   function(){ TPeriod  = "2Y"; toPress(); displayAll(); });
  d3.select("#fourY").on("click",  function(){ TPeriod  = "4Y"; toPress(); displayAll(); });

  d3.select("#MAVG5").on("click",   function(){ TMAVG  = "MAVG5"; toPress(); displayAll(); });
  d3.select("#MAVG10").on("click",   function(){ TMAVG  = "MAVG10"; toPress(); displayAll(); });
  d3.select("#MAVG25").on("click",   function(){ TMAVG  = "MAVG25"; toPress(); displayAll(); });
  d3.select("#MAVG50").on("click",   function(){ TMAVG  = "MAVG50"; toPress(); displayAll(); });
  d3.select("#MAVG75").on("click",   function(){ TMAVG  = "MAVG75"; toPress(); displayAll(); });
}

function displayAll() {
    changeClass();
    displayCS();
    displayGen(genData.length-1);
}

function changeClass() {
    d3.select("#DURB").text(TPeriod);
    d3.select("#MAVGB").text(TMAVG);
    if (TPeriod =="1M") {
        d3.select("#oneM").classed("active", true);
        d3.select("#threeM").classed("active", false);
        d3.select("#sixM").classed("active", false);
        d3.select("#oneY").classed("active", false);
        d3.select("#twoY").classed("active", false);
        d3.select("#fourY").classed("active", false);
    } else if (TPeriod =="6M") {
        d3.select("#oneM").classed("active", false);
        d3.select("#threeM").classed("active", false);
        d3.select("#sixM").classed("active", true);
        d3.select("#oneY").classed("active", false);
        d3.select("#twoY").classed("active", false);
        d3.select("#fourY").classed("active", false);
    } else if (TPeriod =="1Y") {
        d3.select("#oneM").classed("active", false);
        d3.select("#threeM").classed("active", false);
        d3.select("#sixM").classed("active", false);
        d3.select("#oneY").classed("active", true);
        d3.select("#twoY").classed("active", false);
        d3.select("#fourY").classed("active", false);
    } else if (TPeriod =="2Y") {
        d3.select("#oneM").classed("active", false);
        d3.select("#threeM").classed("active", false);
        d3.select("#sixM").classed("active", false);
        d3.select("#oneY").classed("active", false);
        d3.select("#twoY").classed("active", true);
        d3.select("#fourY").classed("active", false);
    } else if (TPeriod =="4Y") {
        d3.select("#oneM").classed("active", false);
        d3.select("#threeM").classed("active", false);
        d3.select("#sixM").classed("active", false);
        d3.select("#oneY").classed("active", false);
        d3.select("#twoY").classed("active", false);
        d3.select("#fourY").classed("active", true);
    } else {
        d3.select("#oneM").classed("active", false);
        d3.select("#threeM").classed("active", true);
        d3.select("#sixM").classed("active", false);
        d3.select("#oneY").classed("active", false);
        d3.select("#twoY").classed("active", false);
        d3.select("#fourY").classed("active", false);
        d3.select("#DURB").text(TPeriod);
    };
    if (TMAVG == "MAVG5") {
        d3.select("#MAVG5").classed("active", true);
        d3.select("#MAVG10").classed("active", false);
        d3.select("#MAVG25").classed("active", false);
        d3.select("#MAVG50").classed("active", false);
        d3.select("#MAVG75").classed("active", false);
    } else if (TMAVG == "MAVG10"){
        d3.select("#MAVG5").classed("active", false);
        d3.select("#MAVG10").classed("active", true);
        d3.select("#MAVG25").classed("active", false);
        d3.select("#MAVG50").classed("active", false);
        d3.select("#MAVG75").classed("active", false);
    } else if (TMAVG == "MAVG25"){
        d3.select("#MAVG5").classed("active", false);
        d3.select("#MAVG10").classed("active", false);
        d3.select("#MAVG25").classed("active", true);
        d3.select("#MAVG50").classed("active", false);
        d3.select("#MAVG75").classed("active", false);
    } else if (TMAVG == "MAVG50"){
        d3.select("#MAVG5").classed("active", false);
        d3.select("#MAVG10").classed("active", false);
        d3.select("#MAVG25").classed("active", false);
        d3.select("#MAVG50").classed("active", true);
        d3.select("#MAVG75").classed("active", false);
    } else if (TMAVG == "MAVG75"){
        d3.select("#MAVG5").classed("active", false);
        d3.select("#MAVG10").classed("active", false);
        d3.select("#MAVG25").classed("active", false);
        d3.select("#MAVG50").classed("active", false);
        d3.select("#MAVG75").classed("active", true);
    } else {
        d3.select("#MAVG5").classed("active", false);
        d3.select("#MAVG10").classed("active", false);
        d3.select("#MAVG25").classed("active", true);
        d3.select("#MAVG50").classed("active", false);
        d3.select("#MAVG75").classed("active", false);
        d3.select("#MAVGB").text("MAVG25");
    }
}

function displayCS() {
    var chart       = cschart().Bheight(380);
    d3.select("#chart1").call(chart);
    var chart       = linechart();
    d3.select("#chart1").datum(genData).call(chart);
    var chart       = barchart().mname("response-time").margin(340).MValue("ACCESS");
    d3.select("#chart1").datum(genData).call(chart);
    hoverAll();
}

function hoverAll() {
    d3.select("#chart1").select(".bands").selectAll("rect")
          .on("mouseover", function(d, i) {
              d3.select(this).classed("hoved", true);
              d3.select(".stick"+i).classed("hoved", true);
              d3.select(".candle"+i).classed("hoved", true);
              d3.select(".volume"+i).classed("hoved", true);
              d3.select(".sigma"+i).classed("hoved", true);
              displayGen(i);
              csaccessbar(genData.slice(i)[0], "inline");
          })
          .on("mouseout", function(d, i) {
              d3.select(this).classed("hoved", false);
              d3.select(".stick"+i).classed("hoved", false);
              d3.select(".candle"+i).classed("hoved", false);
              d3.select(".volume"+i).classed("hoved", false);
              d3.select(".sigma"+i).classed("hoved", false);
              displayGen(genData.length-1);
              csaccessbar(genData.slice(i)[0], "none");
          });
}

function displayGen(mark) {
    var header      = csheader();
    d3.select("#infobar").datum(genData.slice(mark)[0]).call(header);
}
